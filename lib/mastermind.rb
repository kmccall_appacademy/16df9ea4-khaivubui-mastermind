require 'byebug'

class Code
  attr_reader :pegs
  PEGS = {
    Red: 'R', Green: 'G', Blue: 'B', Yellow: 'Y', Orange: 'O', Purple: 'P'
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(color_str)
    unless color_str.upcase.chars.all? { |color| PEGS.values.include? color }
      raise 'invalid colors'
    end
    Code.new(color_str.chars)
  end

  def self.random
    random_colors = []
    4.times do
      random_colors << PEGS.values.sample(1).first
    end
    Code.new(random_colors)
  end

  def [](index)
    pegs[index]
  end

  def exact_matches(other_code)
    count = 0
    pegs.each_with_index do |color, index|
      count += 1 if color.upcase == other_code[index].upcase
    end
    count
  end

  def near_matches(other_code)
    count = 0
    pegs.uniq.each do |color|
      if pegs.count(color) > other_code.pegs.count(color)
        count += other_code.pegs.count(color)
      else
        count += pegs.count(color)
      end
    end
    count - exact_matches(other_code)
  end

  def ==(other)
    return false if other.class != Code
    exact_matches(other) == 4
  end
end

class Game
  attr_reader :secret_code

  def initialize(code = nil)
    @secret_code = code if code
    @secret_code = Code.random unless code
  end

  def get_guess
    puts 'please input a guess'
    Code.parse(gets.chomp)
  end

  def display_matches(code)
    near_matches = secret_code.near_matches(code)
    exact_matches = secret_code.exact_matches(code)
    puts "#{near_matches} near matches and #{exact_matches} exact matches"
  end
end

if __FILE__ == $PROGRAM_NAME
  game = nil
  puts 'Would you like to enter a secret_code? (Y/N)'
  answer = gets.chomp.upcase
  if answer == 'Y'
    puts 'What is the secret code?'
    secret_code = gets.chomp
    game = Game.new(Code.parse(secret_code))
  else
    puts 'Random secret code created'
    game = Game.new
  end
  win = false
  until win
    new_guess = game.get_guess
    game.display_matches(new_guess)
    win = true if game.secret_code == new_guess
  end
  puts 'You have WON!!'
end
